'use strict'

function DomainSearchCtrl ($scope, DomainSearchFactory) {
	
	function searchDomains(searchquery) {
		$scope.domains = '';
		$scope.predicate = 'availability';
		$scope.searchquery = searchquery;
		
		var spinner_el = document.querySelector('.spinner');
		angular.element(spinner_el).toggleClass('hidden');
		
		DomainSearchFactory.getDomainInfo(searchquery).then(function(data) {
			console.log(data);
			angular.forEach(data, function(key, value) {
				if (key.domain.indexOf('.') == -1) {
					// console.log(key);
					data.splice(value);
				}
			});
			angular.element(spinner_el).toggleClass('hidden');
			$scope.domains = data;
		})
	}
	
	$scope.search = function(searchquery) {
		searchDomains(searchquery);
	};

}
DomainSearchCtrl.$inject = ['$scope', 'DomainSearchFactory'];

function DomainSearchFactory($http, $q) {
  
  	function getDomainInfo(searchquery) {  		
  		var deferred = $q.defer();
  		$http.defaults.headers.get = {"X-Mashape-Key":"8a96I2XLdkmshoUTtziRS4XdqOifp1iDQ67jsn6JAMIgbuJTmK"};
		$http.get('https://domainr.p.mashape.com/json/search?q='+searchquery)
		.success(function(data) {
			deferred.resolve(data.results);
		})
		.error(function(err) {
			console.log('Error');
			deferred.reject(err);
		});
		return deferred.promise;
  	}

	return {
		getDomainInfo: getDomainInfo
	};

}
DomainSearchFactory.$inject = ['$http', '$q'];

angular
  .module('app', [])
  .factory('DomainSearchFactory', DomainSearchFactory)
  .controller('DomainSearchCtrl', DomainSearchCtrl);

